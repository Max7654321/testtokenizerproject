﻿using System;
using System.Linq;

namespace ConsoleTokenizerSimple
{
    public static class RandomExtension
    {
        private const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const double nullTrap = 0.2;//чем больше, тем больше null-ов

        public static double? NextDoubleOrNull(this Random random, double maxValue)
        {
            var x = (maxValue + nullTrap) * random.NextDouble();
            return x >= maxValue ? null : (double?)x;
        }

        public static string GenerateString(this Random random, int length)
        {
            return new string(Enumerable.Range(1, length).Select(s => chars[random.Next(chars.Length)]).ToArray());
        }
    }
}