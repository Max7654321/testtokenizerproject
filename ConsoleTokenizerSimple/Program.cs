using System;
using System.Diagnostics;
using System.Linq;

namespace ConsoleTokenizerSimple
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();

            sw.Start();

            var t = new Tokenizer(2);
            //var t = new Tokenizer(600);

            sw.Stop();

            Console.WriteLine("Generate = {0}", sw.Elapsed);

            sw.Reset();

            sw.Start();

            var v = t.Variate(0.7);

            sw.Stop();

            Console.WriteLine("Variate  = {0}", sw.Elapsed);

            Console.WriteLine("Combinations = {0}", v.Count());

            Console.ReadKey(); 
        }
    }
}