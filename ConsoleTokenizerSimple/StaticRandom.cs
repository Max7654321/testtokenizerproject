﻿using System;
using System.Threading;

namespace ConsoleTokenizerSimple
{
    public static class StaticRandom
    {
        private static int seed = Environment.TickCount;

        private static readonly ThreadLocal<Random> random = new ThreadLocal<Random>(() => new Random(Interlocked.Increment(ref seed)));

        public static int Next(int minValue, int maxValue)
        {
            return random.Value.Next(minValue, maxValue);
        }

        public static double? NextDoubleOrNull(double maxValue)
        {
            return random.Value.NextDoubleOrNull(maxValue);
        }

        public static string GenerateString(int length)
        {
            return random.Value.GenerateString(length);
        }
    }
}