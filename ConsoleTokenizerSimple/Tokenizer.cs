﻿using System.Collections.Generic;
using System.Linq;

namespace ConsoleTokenizerSimple
{
    struct StringVariant
    {
        public string value;
        public double? weight;

        public StringVariant(string _value, double? _weight)
        {
            value = _value;
            weight = _weight;
        }
    }

    class Tokenizer
    {
        private const int VAR_MAX = 7;

        private const double WEIGHT_MAX = 1.0;

        private const int LENGTH_MAX = 10;

        private List<List<StringVariant>> TokenData;

        private List<StringVariant> Combinations(StringVariant element, List<StringVariant> partOfList)
        {
            return partOfList.Select(x => new StringVariant(element.value + x.value, element.weight * x.weight)).ToList();
        }

        public List<StringVariant> Variate(double defaultWeight)
        {
             var FlatList = TokenData.SelectMany(x => x)
                .Select(y => new StringVariant(y.value, y.weight ?? defaultWeight)).ToList();

            int ListSize = FlatList.Count();

            var Result = Enumerable.Range(0, ListSize-1)
                .AsParallel()
                .Select(x => Combinations(FlatList.ElementAt(x), FlatList.Skip(x+1).Take(ListSize - x - 1).ToList()))
                .SelectMany(y => y)
                .Distinct()
                .OrderByDescending(z => z.weight)
                .ToList();

            return Result;
        }

        public Tokenizer(int numberOfLists)
        {
            TokenData = Enumerable.Range(1, numberOfLists)
                .AsParallel()
                .Select(x => Enumerable.Range(1, StaticRandom.Next(1, VAR_MAX))
                                    .Select(y => new StringVariant(StaticRandom.GenerateString(StaticRandom.Next(1, LENGTH_MAX)), StaticRandom.NextDoubleOrNull(WEIGHT_MAX)))
                                    .ToList<StringVariant>())
                .ToList<List<StringVariant>>();
        }

    }
}